import org.pircbotx.User;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
/*
 * Throws a pie in a user's face. In this command it's simply the person who issues the command
 */

public class PieCommand extends ListenerAdapter{
	public void onMessage(MessageEvent event){
		if(event.getMessage().startsWith("Pie")){
			String sender = event.getUser().getNick();
			String test = String.valueOf(sender);		
			event.getChannel().send().action("Throws a pie in " + test + "'s face");
		}
	}
}
