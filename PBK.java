import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.UtilSSLSocketFactory;
import org.pircbotx.cap.TLSCapHandler;
/*
 * This is the main class for the bot it starts and sets up the bot. 
 */
public class PBK{
        public static void main(String[] args) {
                //Setup this bot
                Configuration configuration = new Configuration.Builder()
                                .setName("PBK") //Set the nick of the bot. 
                                .setLogin("PBK") //login part of hostmask, eg name:login@host
                                .setAutoNickChange(true) //Automatically change nick when the current one is in use
                                .setCapEnabled(true) //Enable CAP features
                                .addCapHandler(new TLSCapHandler(new UtilSSLSocketFactory().trustAllCertificates(), true))
                                .addListener(new FirstCommand()) //makes the stuff listed in FirstCommand.java a command
                                .addListener(new HelpCommand()) //makes the stuff that is listed in HelpCommand.java a command
                                .addListener(new DateAndTimeCommand()) //makes the stuff in DateAndTimeCommand.java a command
                                .addListener(new PieCommand())
                                .addListener(new LeaveChannel())
                                //.addListener(new JoinChannel())
                                .setServerHostname("irc.rizon.net") //join rizon
                                .addAutoJoinChannel("#PBK-Testing") //Join the PBK testing channel
                                .buildConfiguration();

                //bot.connect throws various exceptions for failures
                try {
                        PircBotX bot = new PircBotX(configuration);
                        //Connect to the rizon IRC network
                        bot.startBot();
                } //In your code you should catch and handle each exception seperately,
                //but here we just lump them all togeather for simpliciy
                catch (Exception ex) {
                        ex.printStackTrace();
                }
        }
}