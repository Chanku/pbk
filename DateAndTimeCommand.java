import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;


public class DateAndTimeCommand extends ListenerAdapter{
	public void onMessage(MessageEvent event) throws Exception {
		DateAndTime DT = new DateAndTime();
        if (event.getMessage().startsWith(".date")){
        	event.getChannel().send().message("The Date is " + DT.getDate());
        }
        if(event.getMessage().startsWith(".time")){
        	event.getChannel().send().message("It is currently " + DT.getTime() + " CDT");
        }
}	
}
