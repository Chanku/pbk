import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
/*
 * This is the help command, I have it in it's own class(as with all my other commands) and I would have
 * it have it's own class even if I didn't do this with all my other commands as the help command is a 
 * rather large command in itself
 */

public class HelpCommand extends ListenerAdapter{
	public void onMessage(MessageEvent event) throws Exception {
        if (event.getMessage().startsWith(".help")){
        	event.getChannel().send().message("Welcome to the help menu of Project BookKeep");
        	event.getChannel().send().message(".date - reports the time");
        	event.getChannel().send().message(".time - reports the date");
        	event.getChannel().send().message(".pie - throws pie in your face");
        	event.getChannel().send().message(".leave - leaves the channel");
        }
	}	
}
