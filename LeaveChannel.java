import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
/*
 * Leaves the channel it is in
 */

public class LeaveChannel extends ListenerAdapter{
	public void onMessage(MessageEvent event){
		if(event.getMessage().startsWith(".leave")){
			event.getChannel().send().part("Goodbye");
		}
	}
}
