import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/*
 * This simply gets the date and time for the Date and time command(found in the class DateandTimeCommand)
 */

public class DateAndTime {
	private String yourDate;
	private String yourTime;
	public String getDate(){
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		Date date = new Date();
		yourDate = dateFormat.format(date);
		return yourDate;
	}
	public String getTime(){
		DateFormat dF = new SimpleDateFormat("HH:mm");
		Date time = new Date();
		yourTime = dF.format(time);
		return yourTime;
	}
}
