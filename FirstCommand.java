import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.types.GenericMessageEvent;
/*
 * First command I ever wrote for the bot it just prints out Hello World to the channel
 */
public class FirstCommand extends ListenerAdapter{

    public void onGenericMessage(final GenericMessageEvent event) throws Exception {
            //Hello world
            //This way to handle commands is useful for listeners that listen for multiple commands
            if (event.getMessage().startsWith(".hello")){
                    event.respond("Hello World!");
            }
    }

}

